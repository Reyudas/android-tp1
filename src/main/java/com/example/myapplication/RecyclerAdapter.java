package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private ArrayList<String> animalsNames = new ArrayList<String>();
    private Context context;

    public RecyclerAdapter(Context context, ArrayList<String> animalsNames) {
        this.animalsNames = animalsNames;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater myInflater = LayoutInflater.from(context);
        View view = myInflater.inflate(R.layout.animal_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        // Name of the animal
        String name = animalsNames.get(position);
        Animal animal = AnimalList.getAnimal(name);

        holder.nameAnimal.setText(name);
        holder.imageAnimal.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier(animal.getImgFile(), "drawable", context.getPackageName())));

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AnimalActivity.class);
                intent.putExtra("name", animalsNames.get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {

        Log.d("getItemCount", "one");
        return animalsNames.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageAnimal;
        TextView nameAnimal;
        RelativeLayout parentLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageAnimal = itemView.findViewById(R.id.imageA);
            nameAnimal = itemView.findViewById(R.id.nameA);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }
}
