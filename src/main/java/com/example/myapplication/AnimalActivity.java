package com.example.myapplication;

//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class AnimalActivity extends AppCompatActivity {

    ImageView img;
    String name;
    Animal animal;
    TextView lifespan;
    TextView gestationPeriod;
    TextView birthWeight;
    TextView adultWeight;
    TextView conservationStatus;
    TextView AName;
    Button saveButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        this.img = findViewById(R.id.imageView);

        this.name = getIntent().getStringExtra("name");
        this.animal = AnimalList.getAnimal(name);

        this.AName = findViewById(R.id.AName);
        this.AName.setText(this.name);

        this.img.setImageDrawable(getResources().getDrawable(getResources().getIdentifier(animal.getImgFile(),"drawable",getPackageName())));

        this.lifespan = findViewById(R.id.lifespan);
        this.lifespan.setText(this.animal.getStrHightestLifespan());

        this.gestationPeriod = findViewById(R.id.gestationPeriod);
        this.gestationPeriod.setText(this.animal.getStrGestationPeriod());

        this.birthWeight = findViewById(R.id.birthWeight);
        this.birthWeight.setText(this.animal.getStrBirthWeight());

        this.adultWeight= findViewById(R.id.adultWeightadultWeight);
        this.adultWeight.setText(this.animal.getStrAdultWeight());

        this.conservationStatus= findViewById(R.id.conservationStatus);
        this.conservationStatus.setText(this.animal.getConservationStatus());

        this.saveButton= findViewById(R.id.saveButton);

        this.saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                String tmp;
                tmp = conservationStatus.getText().toString();
                animal.setConservationStatus(tmp);
            }
        });

    }
}
